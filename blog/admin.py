from polymorphic.admin import PolymorphicInlineSupportMixin, StackedPolymorphicInline

from django.contrib import admin

from blog.models import Page, VideoContent, ContentBase, TextContent, AudioContent

class VideoContentInline(StackedPolymorphicInline.Child):
    model = VideoContent


class TextContentInline(StackedPolymorphicInline.Child):
    model = TextContent


class AudioContentInline(StackedPolymorphicInline.Child):
    model = AudioContent


class PageContentInline(StackedPolymorphicInline):
    model = ContentBase
    child_inlines = (
        VideoContentInline,
        TextContentInline,
        AudioContentInline,
    )

    ordering = ('position',)


@admin.register(Page)
class PageAdmin(PolymorphicInlineSupportMixin, admin.ModelAdmin):
    inlines = (PageContentInline,)
    list_display = ('title',)
    model = Page
    search_fields = ('title', 'content__title')
