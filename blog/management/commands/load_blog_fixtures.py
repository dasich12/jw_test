from django.core.management import call_command
from django.core.management.base import BaseCommand

from blog.models import Page


class Command(BaseCommand):

    def handle(self, *args, **options):
        if not Page.objects.count():
            call_command('loaddata', 'blog_fixtures', app_label='blog')
