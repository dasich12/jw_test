from polymorphic.models import PolymorphicModel

from django.db import models

class Page(models.Model):
    title = models.CharField(u'Заголовок', max_length=200, )

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-pk']
        verbose_name = u'Страница'
        verbose_name_plural = u'Страницы'

def NON_POLYMORPHIC_CASCADE(collector, field, sub_objs, using):
    return models.CASCADE(collector, field, sub_objs.non_polymorphic(), using)

class ContentBase(PolymorphicModel):
    page = models.ForeignKey(Page, related_name='content', on_delete=NON_POLYMORPHIC_CASCADE)
    title = models.CharField(u'Заголовок', max_length=200, )
    counter = models.IntegerField(u'Количество просмотров', default=0)
    position = models.PositiveSmallIntegerField(u'Порядок', default=1)

    class Meta:
        ordering = ['position', 'pk']
        verbose_name = u'Контент'
        verbose_name_plural = u'Контент'

class VideoContent(ContentBase):
    video_url = models.CharField(u'Ссылка на видеофайл', max_length=500, )
    subtitles_url = models.CharField(u'Ссылка на файл субтитров', max_length=200, blank=True)

    def __str__(self):
        return u'Блок видео'

    class Meta:
        verbose_name = u'Видео'
        verbose_name_plural = u'Видео'

class TextContent(ContentBase):
    text = models.TextField(u'Текст',)

    def __str__(self):
        return u'Блок текста'

    class Meta:
        verbose_name = u'Текст'
        verbose_name_plural = u'Текст'


BPM_CHOICES=((1, '32'),
             (2, '96'),
             (3, '128'),
             (4, '192'),
             (5, '256'),
             (6, '320'),)

class AudioContent(ContentBase):
    audio_url = models.CharField(u'Ссылка на аудиофайл', max_length=500,)
    bitrate = models.IntegerField(u'Битрейт (кбит/с)', choices=BPM_CHOICES, default=1)

    def __str__(self):
        return u'Блок аудио'

    class Meta:
        verbose_name = u'Аудио'
        verbose_name_plural = u'Аудио'