from rest_framework import serializers
from rest_polymorphic.serializers import PolymorphicSerializer

from blog.models import Page, TextContent, VideoContent, AudioContent

class AudioContentSerializer(serializers.ModelSerializer):
    bitrate = serializers.SerializerMethodField()

    def get_bitrate(self, obj):
        return obj.get_bitrate_display()

    class Meta:
        model = AudioContent
        fields = ['title', 'counter', 'audio_url', 'bitrate']



class TextContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextContent
        fields = ['title', 'counter', 'text']


class VideoContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoContent
        fields = ['title', 'counter', 'video_url', 'subtitles_url']

class ContentSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        TextContent: TextContentSerializer,
        VideoContent: VideoContentSerializer,
        AudioContent: AudioContentSerializer,
    }

class PageSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Page
        fields = ['url', 'title']
        extra_kwargs = {
            'url': {'view_name': 'api-blog:api_blog_pages-detail'},
        }


class PageDetailSerializer(serializers.HyperlinkedModelSerializer):
    content = ContentSerializer(many=True, read_only=True)

    class Meta:
        model = Page
        fields = ['url', 'title', 'content']
        extra_kwargs = {
            'url': {'view_name': 'api-blog:api_blog_pages-detail'},
        }
