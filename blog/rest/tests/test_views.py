from rest_framework import status
from rest_framework.test import APIRequestFactory
from django.test import Client
from django.urls import reverse
from django.test import TestCase
from django.core.paginator import Paginator
from django.conf import settings

from blog.models import Page
from blog.rest.serializers import PageSerializer, PageDetailSerializer


class PageListTest(TestCase):
    fixtures = ['blog_fixtures']

    def test_first_page(self):
        client = Client()
        response = client.get(reverse('api-blog:api_blog_pages-list'))

        factory = APIRequestFactory()
        request = factory.get(reverse('api-blog:api_blog_pages-list'), {})

        pages = Page.objects.all()

        paginator = Paginator(pages, settings.REST_FRAMEWORK['PAGE_SIZE'])

        serializer = PageSerializer(paginator.page(1), many=True, context={'request': request})

        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(response.data['count'], pages.count())
        self.assertEqual(response.data['previous'], None)
        self.assertEqual(response.data['next'],
                         request.build_absolute_uri("%s?page=2" % reverse('api-blog:api_blog_pages-list')))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_second_page(self):
        client = Client()
        url = "%s?page=2" % reverse('api-blog:api_blog_pages-list')
        response = client.get(url)

        factory = APIRequestFactory()
        request = factory.get(url, {})

        pages = Page.objects.all()

        paginator = Paginator(pages, settings.REST_FRAMEWORK['PAGE_SIZE'])

        serializer = PageSerializer(paginator.page(2), many=True, context={'request': request})

        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(response.data['count'], pages.count())
        self.assertEqual(response.data['previous'],
                         request.build_absolute_uri(reverse('api-blog:api_blog_pages-list')))
        self.assertEqual(response.data['next'], None)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail(self):
        pages = Page.objects.all()
        client = Client()
        factory = APIRequestFactory()
        for page in pages:
            with self.subTest():
                url = reverse('api-blog:api_blog_pages-detail', args=[page.pk])
                response = client.get(url)
                request = factory.get(url, {})
                serializer = PageDetailSerializer(page, context={'request': request})
                self.assertEqual(response.data, serializer.data)
