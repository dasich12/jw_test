from rest_framework import viewsets

from blog.rest.serializers import PageSerializer, PageDetailSerializer
from blog.tasks import inc_page_content_view_count
from blog.models import Page


class PageViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Page.objects.all()
    serializer_class = PageSerializer

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return PageDetailSerializer
        return PageSerializer

    def get_object(self):
        obj = super().get_object()
        inc_page_content_view_count.delay(obj.pk)
        return obj