from __future__ import absolute_import, unicode_literals
from celery import shared_task

from django.db.models import F

from blog.models import ContentBase

@shared_task
def inc_page_content_view_count(page_id):
    ContentBase.objects.filter(
        page_id=page_id
    ).all().update(
        counter=F('counter') + 1
    )

