from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from blog.rest.views import PageViewSet

router = routers.DefaultRouter()
router.register(r'pages', PageViewSet, basename= 'api_blog_pages')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-blog/', include((router.urls, 'api-blog'), namespace='api-blog')),
]
